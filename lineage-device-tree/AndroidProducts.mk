#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_rhodep.mk

COMMON_LUNCH_CHOICES := \
    lineage_rhodep-user \
    lineage_rhodep-userdebug \
    lineage_rhodep-eng
