#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from rhodep device
$(call inherit-product, device/motorola/rhodep/device.mk)

PRODUCT_DEVICE := rhodep
PRODUCT_NAME := lineage_rhodep
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g82 5G
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="rhodep_g-user 13 T1SUS33M.1-124-6-13 cd78a release-keys"

BUILD_FINGERPRINT := motorola/rhodep_g/rhodep:13/T1SUS33M.1-124-6-13/cd78a:user/release-keys
